const BlogListing = function () {
    let logging = true;
    const enableLogging = () => logging = true;
    const disableLogging = () => logging = false;

    const log = (...data) => {
        if (logging) {
            console.log(...data);
        }
    }

    const deleteBlog = (element) => {
        log('delete Blog', element, element.data('url'));
        if (confirm('Are you sure, do you want to delete this Blog?')) {
            let response = Blog.deleteBlog(element.data('url'));
            response.then(() => window.location.reload());
        }
    }
    const deleteBlogEvent = () => $('.deleteBlog').on('click', function (event) { deleteBlog($(this)) });

    return {
        init: () => {
            deleteBlogEvent();
        },
        enableLogging: () => enableLogging(),
        disableLogging: () => disableLogging()
    };
}();
class Blog {
    static deleteBlog (url) {
        let headers = new Headers();
		headers.append('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
		return fetch(url, {method: 'DELETE', headers: headers}).then(response => response.json());
	}

	static async sendRequest(url, jsonData) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		let data = { method: 'DELETE', headers: headers, body: this.JSON_to_URLEncoded(jsonData)};
		let request = new Request(url, data);

		return fetch(request).then(response => response.json()).then(response => response).catch(error => error);
	}
    static JSON_to_URLEncoded(element, key, list) {
		var list = list || [];
		if(typeof(element)=='object'){
			for (var idx in element)
				this.JSON_to_URLEncoded(element[idx],key ? key+'['+idx+']':idx, list);
		} else {
			list.push(key+'='+encodeURIComponent(element));
		}
		return list.join('&');
	}

}
$(document).ready(function () {
    BlogListing.init();
});
