<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Admin routes
Route::prefix('/admin')->name('admin/')->namespace('Admin')->group(function(){
    Route::namespace('Auth')->group(function() {
        //All the admin routes will be defined here...
        Route::get('/login', 'LoginController@showLoginForm')->name('login');
        Route::post('/login', 'LoginController@login');
        Route::post('/logout', 'LoginController@logout')->name('logout');
    });

    Route::get('/dashboard', 'HomeController@index')->name('home');

    // Blogs routes
    Route::get('/blog/listing', 'BlogController@index')->name('blog/listing');
    Route::get('/blog/add', 'BlogController@showAddForm')->name('blog/add');
    Route::post('/blog/create', 'BlogController@create')->name('blog/create');
    Route::get('/blog/edit/{id}', 'BlogController@showEditForm')->name('blog/edit');
    Route::post('/blog/update/{id}', 'BlogController@update')->name('blog/update');
    Route::delete('/blog/delete/{id}', 'BlogController@delete')->name('blog/delete');
});
