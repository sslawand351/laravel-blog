<div class="{{ $class ?? 'col-md-6' }}">
    <div class="form-group @error($name) {{ formErrorClass() }} @enderror">
        <label for="{{ $name }}" class="control-label">{{ __($label) }}</label> @if($required ?? null) <span class="text-red">*</span> @endif
        <div class="form-group">
            @foreach($inputs as $input)
            <input name="{{ $name }}" value="{{ $input['value'] }}" {{ isset($input['checked']) && $input['checked'] ? 'checked="checked"' : '' }} type="radio"> {{ __($input['label']) }} &nbsp;&nbsp;&nbsp;&nbsp;
            @endforeach
        </div>
        @include('admin.layout.form.input.error', ['name' => $name])
    </div>
</div>
