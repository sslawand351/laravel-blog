<div class="{{ $class ?? 'col-md-6' }}">
    <div class="form-group @error($name) {{ formErrorClass() }} @enderror">
        <label for="{{ $name }}" class="control-label">{{ __($label) }}</label> @if($required ?? null) <span class="text-red">*</span> @endif
        @if (($value ?? null) && file_exists($value))
            <div class="col-md-12 form-group">
                {{-- <div class="box">
                    <div class="box-body"> --}}
                        <img src="{{ $url }}" alt="{{ $alt }}" width="200">
                      {{-- <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool">
                          <i class="fa fa-times-circle"></i>
                        </button>
                      </div>
                    </div>
                </div> --}}
            </div>
        @endif
        {{-- @else --}}
            <input type="file" name="{{ $name }}" class="form-control"/>
            <p class="help-block">{{ __('Please select image of dimension 2246x893') }}</p>
            @include('admin.layout.form.input.error', ['name' => $name])
        {{-- @endif --}}
    </div>
</div>
