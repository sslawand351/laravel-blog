<div class="{{ $class ?? 'col-md-6' }}">
    <div class="form-group @error($name) {{ formErrorClass() }} @enderror">
        <label for="{{ $name }}" class="control-label">{{ __($label) }}</label> @if($required ?? null) <span class="text-red">*</span> @endif
        <select {{ $multiple ?? '' }} name="{{ $name }}" value="{{ old($name) ?? $value ?? '' }}" class="form-control" id="{{ $name }}" >
            @foreach ($options as $value => $label)
            <option value="{{ $value }}" {{ $value == $selected  ? 'selected' : '' }}>{{ $label }}</option>
            @endforeach
        </select>
        @include('admin.layout.form.input.error', ['name' => $name])
    </div>
</div>
