<div class="{{ $class ?? 'col-md-6' }}">
    <div class="form-group @error($name) {{ formErrorClass() }} @enderror">
        <label for="{{ $name }}" class="control-label">{{ __($label) }}</label> @if($required ?? null) <span class="text-red">*</span> @endif
        <textarea name="{{ $name }}" class="form-control" id="{{ $name }}">{{ old($name) ?? $value ?? '' }}</textarea>
        @include('admin.layout.form.input.error', ['name' => $name])
    </div>
</div>
