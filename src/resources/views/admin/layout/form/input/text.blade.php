<div class="{{ $class ?? 'col-md-6' }}">
    <div class="form-group @error($name) {{ formErrorClass() }} @enderror">
        <label for="{{ $name }}" class="control-label">{{ __($label) }}</label> @if($required ?? null) <span class="text-red">*</span> @endif
        <input type="text" name="{{ $name }}" value="{{ old($name) ?? $value ?? '' }}" class="form-control" id="{{ $name }}" />
        @include('admin.layout.form.input.error', ['name' => $name])
    </div>
</div>
