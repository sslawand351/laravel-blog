<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <link rel="shortcut icon"  href="{{ admin_img('favicon.ico') }}" type="image/x-icon">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ title($title ?? null) }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" type="text/css" href="{{ admin_css('bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ admin_css('font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Datetimepicker -->
    <link rel="stylesheet" type="text/css" href="{{ admin_css('bootstrap-datetimepicker.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" type="text/css" href="{{ admin_css('AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" type="text/css" href="{{ admin_css('_all-skins.min.css') }}">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">{{ appName() }}</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">{{ appName() }}</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ admin_img('user2-160x160.jpg') }}" class="user-image" alt="User Image">
                            <span class="hidden-xs">Alexander Pierce</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                        <!-- <li class="user-header">
                                            <img src="{{ admin_img('user2-160x160.jpg') }}" class="img-circle" alt="User Image">

                                        <p>
                                            Alexander Pierce - Web Developer
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                        </li> -->
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <form action="{{ getAdminUrl('logout') }}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-default btn-flat">Sign out</button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
{{--<!-- <div>
		<a href="{{ getAdminUrl('logout') }}" style="float: right; color: red">Logout</a>
	</div> -->--}}
<!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
        <!-- <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{ admin_img('user2-160x160.jpg') }}" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>Alexander Pierce</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div> -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li>
                    <a href="{{ getAdminUrl('dashboard') }}" title="{{ __('Dashboard') }}">
                        <i class="fa fa-dashboard"></i> <span>{{ __('Dashboard') }}</span>
                    </a>
                </li>
                <li>
                    <a href="#" title="{{ __('Blog') }}">
                        <i class="fa fa-bold"></i> <span>{{ __('Blog') }}</span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active">
                            <a href="{{ getAdminUrl('blog/add') }}" title="{{ __('Add') }}"><i class="fa fa-plus"></i> {{ __('Add') }}</a>
                        </li>
                        <li>
                            <a href="{{ getAdminUrl('blog/listing') }}"><i class="fa fa-list-ul"></i> Listing</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-bold"></i> <span>Banner</span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active">
                            <a href="{{ getAdminUrl('banner/add') }}"><i class="fa fa-plus"></i> Add</a>
                        </li>
                        <li>
                            <a href="{{ getAdminUrl('banner/listing') }}"><i class="fa fa-list-ul"></i> Listing</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
        @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success') }}
        </div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('error') }}
        </div>
        @endif
        @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright © @if (date('Y') != '2020') {{ '2020-'.date('Y') }} @else {{ '2020' }} @endif <a href="{{ url('') }}" title="{{ appName() }}">{{ appName() }}</a>. All rights reserved.</strong>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">

        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->

        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{ admin_js('jquery-2.2.3.min.js') }}"></script>
@if ($ckeditorJsEnabled ?? false)
<!-- CkEditor -->
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
@endif
<!-- Bootstrap 3.3.6 -->
<script src="{{ admin_js('bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ admin_js('fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ admin_js('app.min.js') }}"></script>
<!-- DatePicker -->
<script src="{{ admin_js('moment.js') }}"></script>
<script src="{{ admin_js('bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ admin_js('global.js') }}"></script>
@if ($customJs ?? false)
@foreach ($customJs as $js)
<script src="{{ admin_js('custom/' . $js) }}"></script>
@endforeach
@endif
@yield('additional_js')
</body>

</html>
