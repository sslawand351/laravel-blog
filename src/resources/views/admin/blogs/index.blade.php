@extends('admin.layout.main')

@section('content')
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ __('Blog Listing') }}</h3>

              {{-- <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div> --}}
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th width=300>{{ __('Image') }}</th>
                  <th>{{ __('Title') }}</th>
                  <th>{{ __('Added On') }}</th>
                  <th>{{ __('Action') }}</th>
                </tr>
@foreach ($blogs as $blog)
                <tr>
                  <td><img src="{{ $blog->getThumbnail()->getUrl() }}" alt="{{ $blog->getTitle() }}" title="{{ $blog->getTitle() }}" width="200"></td>
                  <td>{{ $blog->getTitle() }}</td>
                  <td>{{ $blog->created_at->format('Y-m-d') }}</td>
                  <td>
                    <a href="{{ getAdminUrl('blog/edit/'. $blog->id) }}" alt="{{ __('Edit Blog - ') . $blog->getTitle() }}" title="{{ __('Edit Blog - ') . $blog->getTitle() }}">{{ __('Edit') }}</a>
                    <a href="#" title="{{ __('Delete Blog - ') . $blog->getTitle() }}" class="deleteBlog" data-url="{{ getAdminUrl('blog/delete/'. $blog->id) }}">{{ __('Delete') }}</a>
                  </td>
                </tr>
@endforeach
              </tbody></table>
              {{ $blogs->links() }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection
<form action="javascript:void(0)" method="DELETE" id="deleteBlogForm"></form>
