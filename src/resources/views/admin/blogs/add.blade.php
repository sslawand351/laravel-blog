@extends('admin.layout.main')
@section('content')
{{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ __('Add Blog') }}</h3>
            </div>
            <form action="{{ getAdminUrl('blog/create', true) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="box-body">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="row clearfix">
                                @include('admin.layout.form.input.text', ['name' => 'title', 'label' => 'Title', 'required' => true])
                                @include('admin.layout.form.input.text', ['name' => 'url', 'label' => 'Url', 'required' => true])
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="row clearfix">
                                <?php
                                    // $isPublished = [
                                    //     'name' => 'is_published',
                                    //     'label' => 'Do you want to publish/show blog on front end ?',
                                    //     'inputs' => [
                                    //         ['label' => 'Yes', 'value' => 1],
                                    //         ['label' => 'No', 'value' => 0, 'checked' => true]
                                    //     ]
                                    // ];
                                ?>
                                @include('admin.layout.form.input.textarea', ['name' => 'description', 'label' => 'Description', 'class' => 'col-md-12', 'required' => true])
                                {{-- @include('admin.layout.form.input.radio', $isPublished) --}}
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="row clearfix">
                                @include('admin.layout.form.input.text', ['name' => 'page_title', 'label' => 'Page Title', 'required' => true])
                                @include('admin.layout.form.input.select', ['name' => 'tags', 'label' => 'Tags', 'options' => []])
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="row clearfix">
                                @include('admin.layout.form.input.textarea', ['name' => 'short_description', 'label' => 'Short Description', 'required' => true])
                                @include('admin.layout.form.input.textarea', ['name' => 'meta_description', 'label' => 'Meta Description', 'required' => true])
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="row clearfix">
                                @include('admin.layout.form.input.text', ['name' => 'meta_keywords', 'label' => 'Meta keywords', 'required' => true])
                                @include('admin.layout.form.input.file', ['name' => 'image', 'label' => 'Image', 'required' => true])
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="row clearfix">
                                @include('admin.layout.form.input.file', ['name' => 'thumbnail', 'label' => 'Thumbnail', 'required' => true])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-check"></i> {{ __('Save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
