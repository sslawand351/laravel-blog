<?php
$isPublished = [
    'name' => 'is_published',
    'label' => 'Do you want to publish/show blog on front end ?',
    'inputs' => [
        ['label' => 'Yes', 'value' => 1],
        ['label' => 'No', 'value' => 0, 'checked' => true]
    ]
];
?>
@extends('admin.layout.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('Edit Blog') }}</h3>
                </div>
                <form action="{{ getAdminUrl('blog/update/'.$blog->id, true) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <div class="row clearfix">
                                    @include('admin.layout.form.input.text', ['name' => 'title', 'label' => 'Title', 'value' => $blog->getTitle()])
                                    @include('admin.layout.form.input.text', ['name' => 'url', 'label' => 'Url', 'value' => $blog->getUrl()])
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row clearfix">
                                    <?php
                                        // $isPublished = [
                                        //     'name' => 'is_published',
                                        //     'label' => 'Do you want to publish/show blog on front end ?',
                                        //     'inputs' => [
                                        //         ['label' => 'Yes', 'value' => 1],
                                        //         ['label' => 'No', 'value' => 0, 'checked' => true]
                                        //     ]
                                        // ];
                                    ?>
                                    @include('admin.layout.form.input.textarea', ['name' => 'description', 'label' => 'Description', 'class' => 'col-md-12', 'value' => $blog->getDescription()])
                                    {{-- @include('admin.layout.form.input.radio', $isPublished) --}}
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <div class="row clearfix">
                                    @include('admin.layout.form.input.text', ['name' => 'page_title', 'label' => 'Page Title', 'value' => $blog->getPageTitle()])
                                    @include('admin.layout.form.input.select', ['name' => 'tags', 'label' => 'Tags', 'options' => []])
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row clearfix">
                                    @include('admin.layout.form.input.textarea', ['name' => 'short_description', 'label' => 'Short Description', 'value' => $blog->getShortDescription()])
                                    @include('admin.layout.form.input.textarea', ['name' => 'meta_description', 'label' => 'Meta Description', 'value' => $blog->getMetaDescription()])
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <div class="row clearfix">
                                    @include('admin.layout.form.input.text', ['name' => 'meta_keywords', 'label' => 'Meta keywords', 'value' => $blog->getMetaKeywords()])
                                    @include('admin.layout.form.input.file', array_merge([
                                        'name' => 'image',
                                        'label' => 'Image',
                                    ], $blog->getImage() ? [
                                        'value' => $blog->getImage()->getAbsolutePath(),
                                        'url' => $blog->getImage()->getUrl(),
                                        'alt' =>  $blog->getTitle()
                                    ] : []))
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <div class="row clearfix">
                                    @include('admin.layout.form.input.file', array_merge([
                                        'name' => 'thumbnail',
                                        'label' => 'Thumbnail',
                                    ], $blog->getThumbnail() ? [
                                        'value' => $blog->getThumbnail()->getAbsolutePath(),
                                        'url' => $blog->getThumbnail()->getUrl(),
                                        'alt' =>  $blog->getTitle()
                                    ] : []))
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-check"></i> {{ __('Update') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
