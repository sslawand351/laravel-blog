<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\{CreateBlogRequest, UpdateBlogRequest};
use App\Repository\BlogRepositoryInterface;
use App\Service\Blog\{CreateBlog, UpdateBlog, DeleteBlog};
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    private $blogRepository;
    private $createBlog;
    private $updateBlog;
    private $deleteBlog;

    public function __construct(
        BlogRepositoryInterface $blogRepository,
        CreateBlog $createBlog,
        UpdateBlog $updateBlog,
        DeleteBlog $deleteBlog
    ) {
        $this->middleware(['auth:admin', 'preventBackHistory']);
        $this->blogRepository = $blogRepository;
        $this->createBlog = $createBlog;
        $this->updateBlog = $updateBlog;
        $this->deleteBlog = $deleteBlog;
    }

    public function index()
    {
        Log::info('Get all posts, ordered by the newest first');
        $blogs = $this->blogRepository->paginate(1);

        // Pass Blog Collection to view
        return view('admin.blogs.index', [
            'blogs' => $blogs,
            'title' => __('Blog Listing'),
            'customJs' => [
                'BlogListing.js'
            ]
        ]);
    }

    public function showAddForm()
    {
        return view('admin.blogs.add', [
            'title' => __('Blog Add'),
            'ckeditorJsEnabled' => true,
            'customJs' => [
                'addBlog.js'
            ]
        ]);
    }

    public function create(CreateBlogRequest $request)
    {
        try {
            $request->validated();
            /** CreateBlogResponse/Success|CreateBlogResponse/Failure $createBlogResponse */
            $createBlogResponse = ($this->createBlog)($request);
            if ($createBlogResponse->matchSuccess()) {
                Session::flash('success', __('Blog has added successfully'));
            } else {
                Session::flash('error', $createBlogResponse->errorMessage());
            }
        } catch (Exception $exception) {
            Session::flash('error', $exception->getMessage());
        }
        return redirect()->route('admin/blog/listing');
    }

    public function showEditForm(int $id)
    {
        Log::info('Get blog');
        try {
            $blog = $this->blogRepository->get($id);
        } catch (Exception $exception) {
            Session::flash('error', $exception->getMessage());
            return redirect()->route('admin/blog/listing');
        }

        return view('admin.blogs.edit', [
            'blog' => $blog,
            'title' => __('Blog Edit'),
            'ckeditorJsEnabled' => true,
            'customJs' => [
                'editBlog.js'
            ]
        ]);
    }

    public function update(UpdateBlogRequest $request, int $id)
    {
        try {
            $blog = $this->blogRepository->get($id);
            $request->validated();
            $updateBlogResponse = ($this->updateBlog)($id, $request);
            if ($updateBlogResponse->matchSuccess()) {
                Session::flash('success', __('Blog has updated successfully'));
            } else {
                Session::flash('error', $updateBlogResponse->errorMessage());
            }
        } catch (\Exception $exception) {
            Session::flash('error', $exception->getMessage());
        }
        return redirect()->route('admin/blog/listing');
    }

    public function delete(int $id, Request $request)
    {
        $deleteBlogResponse = ($this->deleteBlog)($id);
        if ($deleteBlogResponse->matchSuccess()) {
            Session::flash('success', __('Blog has deleted successfully'));
            return ['success' => true];
        }
        Session::flash('error', $deleteBlogResponse->errorMessage());
        return ['success' => false];
        // return redirect()->route('admin/blog/listing');
    }
}
