<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Repository\BlogRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BlogController extends Controller
{
    private $blogRepository;

    public function __construct(BlogRepositoryInterface $blogRepository)
    {
        $this->blogRepository = $blogRepository;
    }

    public function index()
    {
        Log::info('Get all posts, ordered by the newest first');
        $blogs = $this->blogRepository->all();

        // Pass Blog Collection to view
        return view('blogs.index', compact('blogs'));
    }
}
