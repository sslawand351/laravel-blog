<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBlogRequest extends FormRequest
{

    // public function __invoke(int $id)
    // {
    //     $this->blogRepository->get($id);
    // }

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => 'required|unique:blogs,id',
            'url' => 'required',
            'description' => 'required',
            'short_description' => 'required',
            'meta_description' => 'required',
            'page_title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'thumbnail' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
        return [];
    }
}
