<?php

if (! function_exists('matchCurrentUrl')) {
    function matchCurrentUrl(string $routeName): bool
    {
        return url()->current() === url($routeName);
    }
}

if (! function_exists('getUrl')) {
    function getUrl(string $routeName, bool $forceUrl = false): string
    {
        return !$forceUrl && matchCurrentUrl($routeName) ? 'javascript:void(0)' : url($routeName);
    }
}

if (! function_exists('getAdminUrl')) {
    function getAdminUrl(string $routeName, bool $forceUrl = false): string
    {
        return getUrl('admin/'.$routeName, $forceUrl);
    }
}

if (! function_exists('admin_css')) {
    function admin_css(string $filepath, ?bool $secure = null): string
    {
        return asset('admin/css/'.$filepath, $secure);
    }
}

if (! function_exists('admin_js')) {
    function admin_js(string $filepath, ?bool $secure = null): string
    {
        return asset('admin/js/'.$filepath, $secure);
    }
}

if (! function_exists('admin_img')) {
    function admin_img(string $imagePath, ?bool $secure = null): string
    {
        return asset('admin/img/'.$imagePath, $secure);
    }
}
