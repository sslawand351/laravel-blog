<?php

if (! function_exists('title')) {
    function title(?string $title, $position = "right"): string
    {
        $appName = config('app.name');
        return $title ? $position == 'right' ? $title . ' - ' . $appName : $appName . ' - ' . $title : $appName;
    }
}

if (! function_exists('appName')) {
    function appName(): string
    {
        return config('app.name');
    }
}

if (! function_exists('formErrorClass')) {
    function formErrorClass(?string $class = null): string
    {
        return $class ?? 'has-error';
    }
}
