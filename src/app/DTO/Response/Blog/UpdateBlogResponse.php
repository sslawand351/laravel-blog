<?php

namespace App\DTO\Response\Blog {

//    use Krak\ADT\ADT;
    use App\Blog;

    abstract class UpdateBlogResponse
    {
        public static function types(): array
        {
            return [UpdateBlogResponse\Success::class, UpdateBlogResponse\Failure::class];
        }

        public static function success(Blog $blog): self
        {
            return new UpdateBlogResponse\Success($blog);
        }

        public function matchSuccess(): bool
        {
            return $this instanceof UpdateBlogResponse\Success;
        }

        public static function failure(string $code, string $errorMessage): self
        {
            return new UpdateBlogResponse\Failure($code, $errorMessage);
        }

        public function matchFailure(): bool
        {
            return $this instanceof UpdateBlogResponse\Failure;
        }
    }
}

namespace App\DTO\Response\Blog\UpdateBlogResponse {

    use App\Blog;
    use App\DTO\Response\Blog\UpdateBlogResponse;

    final class Success extends UpdateBlogResponse
    {
        private $blog;
        private $responseCode;

        public function __construct(Blog $blog)
        {
            $this->responseCode = 'success';
            $this->blog = $blog;
        }

        public function blog(): Blog
        {
            return $this->blog;
        }

        public function responseCode(): string
        {
            return $this->responseCode;
        }

        public function toArray(): array
        {
            return [$this->blog, $this->responseCode];
        }
    }

    final class Failure extends UpdateBlogResponse
    {
        private $responseCode;
        private $errorMessage;

        public function __construct(string $responseCode, string $errorMessage)
        {
            $this->responseCode = $responseCode;
            $this->errorMessage = $errorMessage;
        }

        public function responseCode(): string
        {
            return $this->responseCode;
        }

        public function errorMessage(): string
        {
            return $this->errorMessage;
        }

        public function toArray(): array
        {
            return [$this->responseCode, $this->errorMessage];
        }
    }
}
