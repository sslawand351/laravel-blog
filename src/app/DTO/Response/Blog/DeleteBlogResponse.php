<?php

namespace App\DTO\Response\Blog {

//    use Krak\ADT\ADT;
    use App\Blog;

    abstract class DeleteBlogResponse
    {
        public static function types(): array
        {
            return [DeleteBlogResponse\Success::class, DeleteBlogResponse\Failure::class];
        }

        public static function success(): self
        {
            return new DeleteBlogResponse\Success();
        }

        public function matchSuccess(): bool
        {
            return $this instanceof DeleteBlogResponse\Success;
        }

        public static function failure(string $code, string $errorMessage): self
        {
            return new DeleteBlogResponse\Failure($code, $errorMessage);
        }

        public function matchFailure(): bool
        {
            return $this instanceof DeleteBlogResponse\Failure;
        }
    }
}

namespace App\DTO\Response\Blog\DeleteBlogResponse {

    use App\Blog;
    use App\DTO\Response\Blog\DeleteBlogResponse;

    final class Success extends DeleteBlogResponse
    {
        private $blog;
        private $responseCode;

        public function __construct()
        {
            $this->responseCode = 'success';
        }

        public function responseCode(): string
        {
            return $this->responseCode;
        }

        public function toArray(): array
        {
            return [$this->responseCode];
        }
    }

    final class Failure extends DeleteBlogResponse
    {
        private $responseCode;
        private $errorMessage;

        public function __construct(string $responseCode, string $errorMessage)
        {
            $this->responseCode = $responseCode;
            $this->errorMessage = $errorMessage;
        }

        public function responseCode(): string
        {
            return $this->responseCode;
        }

        public function errorMessage(): string
        {
            return $this->errorMessage;
        }

        public function toArray(): array
        {
            return [$this->responseCode, $this->errorMessage];
        }
    }
}
