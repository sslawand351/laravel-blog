<?php

namespace App\DTO\Response\Blog {

//    use Krak\ADT\ADT;
    use App\Blog;

    abstract class CreateBlogResponse
    {
        public static function types(): array
        {
            return [CreateBlogResponse\Success::class, CreateBlogResponse\Failure::class];
        }

        public static function success(Blog $blog): self
        {
            return new CreateBlogResponse\Success($blog);
        }

        public function matchSuccess(): bool
        {
            return $this instanceof CreateBlogResponse\Success;
        }

        public static function failure(string $code, string $errorMessage): self
        {
            return new CreateBlogResponse\Failure($code, $errorMessage);
        }

        public function matchFailure(): bool
        {
            return $this instanceof CreateBlogResponse\Failure;
        }
    }
}

namespace App\DTO\Response\Blog\CreateBlogResponse {

    use App\Blog;
    use App\DTO\Response\Blog\CreateBlogResponse;

    final class Success extends CreateBlogResponse
    {
        private $blog;
        private $responseCode;

        public function __construct(Blog $blog)
        {
            $this->responseCode = 'success';
            $this->blog = $blog;
        }

        public function blog(): Blog
        {
            return $this->blog;
        }

        public function responseCode(): string
        {
            return $this->responseCode;
        }

        public function toArray(): array
        {
            return [$this->blog, $this->responseCode];
        }
    }

    final class Failure extends CreateBlogResponse
    {
        private $responseCode;
        private $errorMessage;

        public function __construct(string $responseCode, string $errorMessage)
        {
            $this->responseCode = $responseCode;
            $this->errorMessage = $errorMessage;
        }

        public function responseCode(): string
        {
            return $this->responseCode;
        }

        public function errorMessage(): string
        {
            return $this->errorMessage;
        }

        public function toArray(): array
        {
            return [$this->responseCode, $this->errorMessage];
        }
    }
}
