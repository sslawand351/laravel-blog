<?php

namespace App\Service\Assets;

class Assets
{
    public static function js(string $filename): string
    {
        $file_path = FCPATH .'resources/'. $filename;
        if (file_exists($file_path) && $filename != null) {
            return '<script type="text/javascript" src="'. url('resources/'. $filename) .'"></script>';
        }
        return '<!-- file '. url('resources/'. $filename) .' not found -->';
    }
     
    public static function css(string $filename): string
    {
        $file_path = FCPATH . 'resources/' . $filename;
        if (file_exists($file_path) && $filename != null) {
            return '<link rel="stylesheet" media="all" href="'. url('resources/'. $filename) .'" />';
        }
        return '<!-- file '. url('resources/'.$filename) .' not found -->';
    }
 
    public static function img($filename, $attributes=[], bool $tag=true): string
    {
        if ($tag) {
            return url('resources/img/'. $filename);
        }
        $attributesString = "";
        foreach ($attributes as $key => $value) {
            $attributesString .= $key.'="'. $value .'" ';
        }
        return '<img src="'. url('resources/'. $filename) .'" '.$attributesString.' />';
    }
}
