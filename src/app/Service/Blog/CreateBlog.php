<?php

namespace App\Service\Blog;

use App\Blog;
use App\Http\Requests\Blog\CreateBlogRequest;
use App\DTO\Response\Blog\CreateBlogResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

final class CreateBlog
{
    public function __invoke(CreateBlogRequest $request)
    {
        if (!($validate = $request->validated())) {
            return $validate;
        }
        DB::beginTransaction();
        try {
            $request->imageName = $this->getImageName($request, 'image');
            $request->thumbnailName = $this->getImageName($request, 'thumbnail');

            $blog = Blog::createFromRequest($request);
            $blog->save();
            $request->image->move($this->getImagePath($blog), $request->imageName);
            $request->thumbnail->move($this->getThumbnailPath($blog), $request->thumbnailName);
        } catch (\Exception $exception) {
            DB::rollBack();
            if ($blog->id) {
                File::deleteDirectories($this->getImagePath($blog));
                File::deleteDirectories($this->getThumbnailPath($blog));
            }
            return CreateBlogResponse::failure('failure', $exception->getMessage());
        }
        DB::commit();
        return CreateBlogResponse::success($blog);
    }

    private function getImageName(CreateBlogRequest $request, string $inputName): string
    {
        $filename = pathinfo($request->file($inputName)->getClientOriginalName(), PATHINFO_FILENAME);
        return md5(time() . $filename) .'.'. $request->file($inputName)->extension();
    }

    private function getImagePath(Blog $blog): string
    {
        $path = public_path($blog->getImagePath());
        $this->makeDirectory($path);
        return $path;
    }

    private function getThumbnailPath(Blog $blog): string
    {
        $path = public_path($blog->getThumbnailPath());
        $this->makeDirectory($path);
        return $path;
    }

    private function makeDirectory(string $path): void
    {
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
    }
}
