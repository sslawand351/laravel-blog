<?php

namespace App\Service\Blog;

use App\Blog;
use App\Http\Requests\Blog\UpdateBlogRequest;
use App\Repository\BlogRepository;
use App\DTO\Response\Blog\UpdateBlogResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

final class UpdateBlog
{
    private $blogRepository;
    private $oldImagePaths = [];

    public function __construct(BlogRepository $blogRepository)
    {
        $this->blogRepository = $blogRepository;
    }

    public function __invoke(int $id, UpdateBlogRequest $request)
    {
        if (!($validate = $request->validated())) {
            return $validate;
        }
        DB::beginTransaction();
        try {
            $blog = $this->blogRepository->get($id);
            if ($request->file('image')) {
                $request->imageName = $this->getImageName($request, 'image');
                $this->oldImagePaths[] = $blog->getImage()->getAbsolutePath();
                $request->image->move($this->getImagePath($blog), $request->imageName);
            }

            if ($request->file('thumbnail')) {
                $request->thumbnailName = $this->getImageName($request, 'thumbnail');
                $this->oldImagePaths[] = $blog->getThumbnail()->getAbsolutePath();
                $request->thumbnail->move($this->getThumbnailPath($blog), $request->thumbnailName);
            }

            $blog->updateFromRequest($request);
            $blog->save();
        } catch (\Exception $exception) {
            DB::rollBack();
            $request->imageName ? File::delete($this->getImagePath($blog) .'/'. $request->imageName) : null;
            $request->thumbnailName ? File::delete($this->getThumbnailPath($blog) .'/'. $request->thumbnailName) : null;
            return UpdateBlogResponse::failure('failure', $exception->getMessage());
        }
        DB::commit();

        $this->deleteOldImages();
        return UpdateBlogResponse::success($blog);
    }

    private function deleteOldImages(): void
    {
        if (count($this->oldImagePaths) > 0) {
            foreach ($this->oldImagePaths as $imagePath) {
                File::delete($imagePath);
            }
        }
    }

    private function getImageName(UpdateBlogRequest $request, string $inputName): string
    {
        $filename = pathinfo($request->file($inputName)->getClientOriginalName(), PATHINFO_FILENAME);
        return md5(time() . $filename) .'.'. $request->file($inputName)->extension();
    }

    private function getImagePath(Blog $blog): string
    {
        $path = public_path($blog->getImagePath());
        $this->makeDirectory($path);
        return $path;
    }

    private function getThumbnailPath(Blog $blog): string
    {
        $path = public_path($blog->getThumbnailPath());
        $this->makeDirectory($path);
        return $path;
    }

    private function makeDirectory(string $path): void
    {
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
    }
}
