<?php

namespace App\Service\Blog;

use App\DTO\Response\Blog\DeleteBlogResponse;
use App\Repository\BlogRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

final class DeleteBlog
{
    private $blogRepository;
    private $imagePaths = [];

    public function __construct(BlogRepository $blogRepository)
    {
        $this->blogRepository = $blogRepository;
    }

    public function __invoke(int $id)
    {
        DB::beginTransaction();
        try {
            $blog = $this->blogRepository->get($id);
            $this->imagePaths = [$blog->getImage()->getImageDir(), $blog->getThumbnail()->getImageDir()];
            $blog->delete();
        } catch (Exception $exception) {
            DB::rollBack();
            return DeleteBlogResponse::failure('failure', $exception->getMessage());
        }
        DB::commit();
        $this->deleteImages();
        return DeleteBlogResponse::success();
    }

    public function deleteImages(): void
    {
        foreach ($this->imagePaths as $imageDir) {
            File::deleteDirectory($imageDir);
        }
    }
}
