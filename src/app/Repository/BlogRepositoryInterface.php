<?php

namespace App\Repository;

use App\Blog;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

interface BlogRepositoryInterface
{
    public function get(int $blogId): Blog;
    /** @return mixed */
    public function all(): Collection;
    public function paginate(int $count): Paginator;
    public function delete(int $blogId): int;
    public function update(int $blogId, array $data);
}
