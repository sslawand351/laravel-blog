<?php

namespace App\Repository;

use App\Blog;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

class BlogRepository implements BlogRepositoryInterface
{
    public function get(int $blogId): Blog
    {
        $blog = Blog::find($blogId);
        if (!$blog) {
            throw new \RuntimeException('Blog does not exist with id: ' . $blogId);
        }
        return $blog;
    }

    public function all(): Collection
    {
        return Blog::all();
    }

    public function paginate(int $count): Paginator
    {
        return Blog::orderBy('id', 'DESC')->paginate($count);
    }

    public function delete(int $blogId): int
    {
        return Blog::destroy($blogId);
    }

    public function update(int $blogId, array $data)
    {
        Blog::find($blogId)->update($data);
    }
}
