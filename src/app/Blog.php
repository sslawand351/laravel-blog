<?php

namespace App;

use App\Http\Requests\Blog\{CreateBlogRequest, UpdateBlogRequest};
use App\Image\Image;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $privateProperties = ['title'];
    private $title;
    private $url;
    private $category_id;
    private $description;
    private $short_description;
    private $meta_description;
    private $meta_keywords;
    private $created_by;
    private $updated_by;
    // protected $fillable = ['title', 'description', 'url'];
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
    const IMAGE_PATH = 'blogs/images/';
    const THUMBNAIL_PATH = 'blogs/thumbnail/';

    // public function __construct()
    // {
    //     parent::__construct();
    //     $this->setProperties();
    // }

    public static function createFromRequest(CreateBlogRequest $request)
    {
        // $blogData = [
        //     'title' => $request->get('title'),
        //     'description' => $request->get('description') ?? null,
        //     'url' => $request->get('url') ?? null,
        //     'short_description' => $request->get('short_description') ?? null,
        //     'meta_description' => $request->get('meta_description') ?? null,
        //     // 'meta_keywords' => $request->get('meta_keywords') ?? null,
        //     // 'created_by' => $request->get('created_by'),
        //     // 'updated_by' => $request->get('updated_by'),
        // ];
        // dump($blogData);
        // return self::create($blogData);
        $self = new self;
        $self->title = $request->get('title');
        $self->url = $request->get('url') ?? null;
        $self->category_id = $request->get('category_id') ?? 1;
        $self->description = $request->get('description') ?? null;
        $self->short_description = $request->get('short_description') ?? null;
        $self->page_title = $request->get('page_title') ?? null;
        $self->meta_description = $request->get('meta_description') ?? null;
        $self->meta_keywords = $request->get('meta_keywords') ?? null;
        $self->image_name = $request->imageName ?? null;
        $self->thumbnail_name = $request->thumbnailName ?? null;
        $self->created_by = $request->user()->id;
        $self->updated_by = $request->user()->id;
        $self->setAttributes([
            'title' => $self->title,
            'url' => $self->url,
            'category_id' => $self->category_id,
            'description' => $self->description,
            'short_description' => $self->short_description,
            'meta_description' => $self->meta_description,
            'meta_keywords' => $self->meta_keywords,
            'created_by' => $self->created_by,
            'updated_by' => $self->updated_by,
        ]);
        $self->updateTimestamps();
        return $self;
    }

    public function updateFromRequest(UpdateBlogRequest $request)
    {
        $this->title = $request->get('title');
        $this->url = $request->get('url') ?? $this->url;
        $this->category_id = $request->get('category_id') ?? 1;
        $this->description = $request->get('description') ?? $this->description;
        $this->short_description = $request->get('short_description') ?? $this->short_description;
        $this->page_title = $request->get('page_title') ?? $this->page_title;
        $this->meta_description = $request->get('meta_description') ?? $this->meta_description;
        $this->meta_keywords = $request->get('meta_keywords') ?? null;
        $this->image_name = $request->imageName ?? $this->image_name;
        $this->thumbnail_name = $request->thumbnailName ?? $this->thumbnail_name;
        $this->updated_by = $request->user()->id;
        $this->setAttributes([
            'title' => $this->title,
            'url' => $this->url,
            'category_id' => $this->category_id,
            'description' => $this->description,
            'short_description' => $this->short_description,
            'meta_description' => $this->meta_description,
            'meta_keywords' => $this->meta_keywords,
            'updated_by' => $this->updated_by,
        ]);
        $this->updateTimestamps();
        return $this;
    }

    public function __get($varName) {
        $this->isPrivate($varName);

        return parent::__get($varName);
    }

    public function __set($varName, $value) {
        $this->isPrivate($varName);

        return parent::__set($varName, $value);
    }

    protected function isPrivate($varName) {
        if (in_array($varName, $this->privateProperties)) {
            throw new \Exception('Uncaught Error: Cannot access private property ' . get_class() . '::$' . $varName);
        }
    }

    private function setProperties()
    {
        dd($this->description);
        foreach ($this->attributes as $key => $value) {
            $this->$key = $value;
        }
    }

    private function setAttributes(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }

    private function setCreatedBy(): self
    {
        $this->created_by = auth('admin')->user()->id;
        return $this;
    }

    private function setUpdatedBy(): self
    {
        $this->updated_by = auth('admin')->user()->id;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->attributes['title'];
        return $this->title;
    }

    public function getUrl(): ?string
    {
        return $this->attributes['url'];
        return $this->url;
    }

    public function getCategoryId(): ?int
    {
        return $this->attributes['category_id'];
        return $this->category_id;
    }

    public function getDescription(): ?string
    {
        return $this->attributes['description'];
        return $this->description;
    }

    public function getShortDescription(): ?string
    {
        return $this->attributes['short_description'];
        return $this->short_description;
    }

    public function getMetaDescription(): ?string
    {
        return $this->attributes['meta_description'];
        return $this->meta_description;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->attributes['meta_keywords'];
        return $this->meta_keywords;
    }

    public function getPageTitle(): ?string
    {
        // return $this->attributes['page_title'];
        return $this->page_title;
    }

    public function getCreatedBy(): ?int
    {
        return $this->attributes['created_by'];
        return $this->created_by;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->attributes['updated_by'];
        return $this->updated_by;
    }

    public function getImage(): ?Image
    {
        return $this->image_name ? new Image(self::IMAGE_PATH.$this->id.'/', $this->image_name) : null;
    }

    public function getImagePath(): string
    {
        return self::IMAGE_PATH.$this->id;
    }

    public function getThumbnail(): ?Image
    {
        return $this->thumbnail_name ? new Image(self::THUMBNAIL_PATH.$this->id.'/', $this->thumbnail_name) : null;
    }

    public function getThumbnailPath(): string
    {
        return self::THUMBNAIL_PATH.$this->id;
    }
}
