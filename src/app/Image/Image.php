<?php

namespace App\Image;

final class Image
{
    private $imagePath;
    private $imageName;

    public function __construct(string $imagePath, string $imageName)
    {
        $this->imagePath = $imagePath;
        $this->imageName = $imageName;
    }

    public function getAbsolutePath(): string
    {
        return public_path($this->getPath());
    }

    public function getImageDir(): string
    {
        return public_path($this->imagePath);
    }

    public function getUrl(): string
    {
        return getUrl($this->getPath());
    }

    public function getPath(): string
    {
        return $this->imagePath . $this->imageName;
    }
}
